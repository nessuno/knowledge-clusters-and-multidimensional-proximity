;Copyright 2015 Carlo Bottai and Martina Iori

;This file is part of knowledge-clusters-and-multidimensional-proximity.

;knowledge-clusters-and-multidimensional-proximity
;is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.

;knowledge-clusters-and-multidimensional-proximity
;is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.

;You should have received a copy of the GNU General Public License
;along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.

breed [traditional-firms traditional-firm] ; classical firms with localized knowledge
breed [innovative-firms innovative-firm] ; firms with spread knowledge and most innovative

undirected-link-breed [inn-inn-links inn-inn-link]
undirected-link-breed [trad-trad-links trad-trad-link]
undirected-link-breed [trad-inn-links trad-inn-link]

globals
[
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; independent variables ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;IPR-strength ; IPR regime : moved to slider
  ;smell-radius ;radius of knowledge diffusion : moved to slider
  ;innovative-firms-proportion ; proportion of innovative firms (breed) : moved to slider
  ;time: number of ticks after which simulation ends: move to interface
  
  technology-groups-num  ; number of possibile technological class
  turtles-num  ; number of firms
  diagonal ; diagonal of world space
  knowledge-steps ; number of possible different pieces of knowledge
  constant ; coefficient of the strength
  more-best-patch-money ; max value of money of all the patches
  max-knowledge-length ; number of pieces of knowledge of the firm with more knowledge
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  auxiliary variables  ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  auxiliary-neighbours-profits ; auxiliar variable that changes for each agent
  auxiliary-public-knowledge ; auxiliar variable that collects the public knowledge in a neighbours
  auxiliary-strength-x ; strength of the attraction along x-axis
  auxiliary-strength-y ; strength of the attraction along y-axis
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;    test variables     ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;  test-innovative-firms-pr ; auxiliar variable : number of firms that innovate after profits control
  ;  test-innovation-happen ;control: firms that innovate

  ; --- cluster formation test ---
  cluster-max-dim ; dimension of the biggest cluster
  cluster-max-dim-plot
  in-cluster-innovative ; number of innovative firms in cluster
  in-cluster-innovative-plot 
  in-cluster-traditional ;number of traditional firms in cluster
  in-cluster-traditional-plot

  links-inn-inn ; number of links between innovative firms / total number of links with one end that is innovative
  links-inn-inn-plot
  links-trad-trad ;number of links between traditional firms /  total number of links with one end that is traditional
  links-trad-trad-plot
  
  ; --- number of innovations in the different breed test ---
  rep-innovation
  rep-innovation-traditional
  rep-innovation-innovative
  
  knowledge-ends ; if it is = to turtles-num it means that all the turtles have no more free knowledge
  chech-plot ; only useful to avoid the program enetrs two times in the procedure
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  Possible extensions  ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; --- clustering: ---
  ;clustering ;initial clustering : possible extension
  ;city-radius ; extension of the cities
  ;city-num ; number of cities around which the firms put themselfs initially if clustering? True
  
  ; --- profit ---
  ;all-same-profit? ; Bool that sets if alla firms have the same initial profit : possible extension
]

turtles-own
[
  RD-inclination  ; inclination to use budget for innovation (from 1 to 5)
  technology  ; technological class of the firm: it is discrete
  knowledge; list of pieces of knowledge created by the firm
  IPR-on-knowledge; list of booleans linked 1-1 to pubblic-knowledge variable
  budget  ; budget that firm can use for innovation
  profits
  profits-threshold ; threshold to have a creative reaction
  ;budget-threshold ; threshold to innovate
  neighbours
  in-cluster? ;it is True if the firm is in a cluster
  my-knowledge-ends ; is True if there is no free space for new pieces of knowledge
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  auxiliary variables  ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  mass ; knowledge quantity (if the firm is traditional it depends only on the knowledge in its sector)
  mass-array ; knowledge quantity for the other firms
  mean-internal-knowledge ; mean point in the firm's internal knowledge
  knowledge-radius ; radius that determines the public knowledge used by the firm
  xcor2 ; auxiliary variable with the xcor at the following tick
  ycor2 ; auxiliary variable with the xcor at the following tick
]

patches-own
[
  used?  ; if it is True one and only one firm is on this patch.
  money 
  consumer? ; if it is 1 this patch is a consumer
]
