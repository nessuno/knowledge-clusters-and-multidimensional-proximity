;Copyright 2015 Carlo Bottai and Martina Iori

;This file is part of knowledge-clusters-and-multidimensional-proximity.

;knowledge-clusters-and-multidimensional-proximity
;is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.

;knowledge-clusters-and-multidimensional-proximity
;is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.

;You should have received a copy of the GNU General Public License
;along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.

to go
  
  if ticks > time [stop]
  tick
  clear-links
  
  ;------preliminary steps-----
  
  ; --- set patch (consumer) money ---
  set more-best-patch-money 0
  ask patches with [consumer? = 1]
  [
    set money money + exp (-1 * money)  ; update money
    if money > more-best-patch-money [set more-best-patch-money money]
  ]
  
  ; --- firms update budget, neighbourhood, mass and IPRs ---
  ask turtles
  [
    ;set profits random-normal profits 1  ;new profits
    set neighbours other turtles in-radius smell-radius
    set budget budget + profits ; all new profits go to increase budget
    
    if knowledge != IPR-on-knowledge
    [
      ; --- Each firm update his IPRs: some of its knowledge becomes free ---
      let k 0
      while [k < knowledge-steps] [
        if (array:item IPR-on-knowledge k) != 0 and (array:item IPR-on-knowledge k) != 1 
        [
          array:set IPR-on-knowledge k ((array:item IPR-on-knowledge k) - 1)
        ]
        set k k + 1
      ]
      set-mass-array
    ]
  ]
  
  ; --- innovation ---
  ask turtles
  [
    search-neighbours-profits
    check-profits-threshold-and-innovate  ; innovation for profits
    ;check-budget-threshold-and-innovate  ; innovation for budget
  ]
  
  ; --- firms look around and search for new opportunities of interchange ---
  ask turtles  
  [ 
    before-move
  ]
  
  ; --- firms change location in the geographical / network space ---
  ask turtles
  [
    move 
  ]
  
  ; --- cluster test ---
  ask turtles
  [
    test-clusters
  ] 
  show-clusters
    
end ; end go procedure

