# Knowledge Clusters and Multidimensional Proximity: An Agent-Based Simulation #

### Authors ###
*Martina Iori*   
Master's student at Collegio Carlo Alberto (Turin)   
martina DOT iori AT carloalberto DOT org

*Carlo Bottai*   
Master's student at Collegio Carlo Alberto (Turin)   
carlo DOT bottai AT carloalberto DOT org

### Abstract ###
We will use an Agent-Based Model in order to study how innovation can emerge
from the interaction between firms. In particular, we are interested in studying how
the clusters that emerges from these interactions influence the ability of bounded
rational firms in reacting creatively to out-of-equilibrium conditions. Moreover, we
will introduce two type of firms – traditional and innovative – and we will observe if
and how this difference influences the outcomes.

### Keywords ###
Spillovers; Agent-Based Model; Industrial clusters.

### Releas Notes ###
Version 1.0

The model in developed in NetLogo ([https://ccl.northwestern.edu/netlogo/](https://ccl.northwestern.edu/netlogo/)) with the Array ([http://ccl.northwestern.edu/netlogo/docs/arraystables.html](http://ccl.northwestern.edu/netlogo/docs/arraystables.html)) and NW ([https://github.com/NetLogo/NW-Extension](https://github.com/NetLogo/NW-Extension)) Extensions.

### Licence ###

Copyright 2015 Carlo Bottai and Martina Iori

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.